﻿#include <iostream>
#include <cmath>

class Vector
{
public:

    Vector() : x(0), y(0), z(0) {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

    void SetVector(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
    }

    void GetVector()
    {
        std::cout << "x: " << x << std::endl << "y: " << y << std::endl << "z: " << z << std::endl;
    }

    double GetModule()
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

private:
    double x;
    double y;
    double z;
};

int main() {

    Vector vector;

    vector.SetVector(3, 5, 4);
    vector.GetVector();

    std::cout << "Module: " << vector.GetModule();

    return 0;
}